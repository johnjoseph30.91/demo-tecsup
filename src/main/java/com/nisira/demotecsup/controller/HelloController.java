package com.nisira.demotecsup.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloController {
	@GetMapping("/hello-world")
	public String returnHello(){
		return "Hello World!";
	}
	@GetMapping("/hello/{name}")
	public String returnHelloWithName(@PathVariable String name){
		return "Hello " + name + "!!!!!!."
	}
	@GetMapping("/sum/{num1}/{num2}")
	public int returnBye(@PathVariable int num1, @PathVariable int num2) {
		return num1 + num2;
	}
	@GetMapping("/bye")
	public String returnBye() {
		return "Bye.";
	}
}
