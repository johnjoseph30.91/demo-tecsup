package com.nisira.demotecsup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoTecsupApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoTecsupApplication.class, args);
	}

}
