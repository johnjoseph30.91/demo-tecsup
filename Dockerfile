FROM openjdk:17-alpine3.12
WORKDIR /app

COPY .mvn/ .mvn
COPY mvnw pom.xml ./

RUN ./mvnw dependency:resolve

COPY src ./src

RUN ./mvnw package

EXPOSE 8080

ENTRYPOINT ["java",  "-jar", "target/demo-tecsup-0.0.1-SNAPSHOT.jar"]
